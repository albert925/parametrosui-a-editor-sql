export function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

export function arrayParamsExec(stringSql) {
  const paramsQueryText = [...stringSql.matchAll(/{{(\w+)}}/ig)];
  const list = paramsQueryText.map(item => item[1]);
  const indexString = paramsQueryText.map(item => item.index);
  return {
    list,
    paramsQueryText,
    indexString
  };
}