import React from 'react';
import PropTypes from 'prop-types';
import {
  DropdownContainer,
  ContentDiv,
  ActionsContainer,
} from './styles';
function DymanicKeyValueInputInput({
  properties,
  updateProperty,
  deleteProperty,
  addProperty,
  id,
  className,
  tilesColumns,
  inputsPlaceholder,
  isIconError,
  textButtonAdd
}) {
  return (
    <>
      {properties.map((property, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <ContentDiv key={`${id}__${index}`} className={(index === 0 && (tilesColumns[0] && tilesColumns[1])) ? `first${className}` : className}>
          <DropdownContainer>
            <input
              placeholder={inputsPlaceholder[0]}
              onChange={e => updateProperty(e, index, 'name')}
              value={property.name}
              id={`${id}-key-${index}`}
            />
          </DropdownContainer>
          <DropdownContainer>
            <input
              placeholder={inputsPlaceholder[1]}
              onChange={e => updateProperty(e, index, 'value')}
              value={property.value}
              id={`${id}-value-${index}`}
            />
          </DropdownContainer>
          <ActionsContainer className={index === 0 ? 'firstClose' : ''}>
            <button
              type="button"
              className="smallMargin"
              onClick={() => deleteProperty(index)}
            >
              X
            </button>
          </ActionsContainer>
        </ContentDiv>
      ))}
      <button onClick={addProperty}>{textButtonAdd}</button>
    </>
  );
}

DymanicKeyValueInputInput.defaultProps = {
  className: '',
  tilesColumns: ['Name', 'Value'],
  inputsPlaceholder: ['Name', 'Value'],
  isIconError: false,
  isIconTransfer: false,
  textButtonAdd: 'Add',
};

DymanicKeyValueInputInput.propTypes = {
  properties: PropTypes.array,
  updateProperty: PropTypes.func,
  deleteProperty: PropTypes.func,
  addProperty: PropTypes.func,
  id: PropTypes.string,
  className: PropTypes.string,
  inputsPlaceholder: PropTypes.array,
  tilesColumns: PropTypes.array,
  isIconError: PropTypes.bool,
  textButtonAdd: PropTypes.string,
};

export default DymanicKeyValueInputInput;
