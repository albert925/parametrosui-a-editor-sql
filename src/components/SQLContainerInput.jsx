/* eslint-disable no-param-reassign */
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import MonacoEditor from 'react-monaco-editor';

import { themeKuroirMonacoEditor } from '../utils/themes';

const SQLContainerInput = ({
  input,
}) => {
  const valueSQL = input?.value ? input?.value : '';
  const refEditor = useRef(null);
  const [valueTextArea, setValueTextArea] = useState('');

  useEffect(() => {
    setValueTextArea(valueSQL);
  }, [valueSQL]);


  const changeTextEditorText = valueD => {
    setValueTextArea(valueD);

    if (input?.onChange) {
      input?.onChange(valueD);
    }
  };

  const editorWillMount = monaco => {
    if (monaco?.editor?.defineTheme) {
      monaco?.editor.defineTheme('kuroir', themeKuroirMonacoEditor);
    }
  };

  const editorDidMount = editor => {
    if (editor?.onDidChangeCursorPosition) {
      editor?.onDidChangeCursorPosition(data => {
        let column = 0;
        let lineNumber = 0;
        if (data?.position?.column) {
          column = data?.position?.column;
        }
        if (data?.position?.lineNumber) {
          lineNumber = data?.position?.lineNumber;
        }

        const insert = {
          startLineNumber: lineNumber,
          startColumn: column,
          endLineNumber: lineNumber,
          endColumn: column,
        };
        console.log(insert)
      });
    }
  };

  return (
    <>
      <MonacoEditor
        width="100%"
        height={200}
        language="mysql"
        theme="kuroir"
        value={valueTextArea}
        ref={refEditor}
        editorDidMount={editorDidMount}
        editorWillMount={editorWillMount}
        options={{
          selectOnLineNumbers: true,
          renderLineHighlight: "none",
          hideCursorInOverviewRuler: true,
          minimap: {
            enabled: false,
          },
          fontSize: 12,
          wordWrap: "on",
          lineHeight: 16,
        }}
        onChange={changeTextEditorText}
      />
    </>
  );
};

SQLContainerInput.propTypes = {
  input: PropTypes.object,
};

export default SQLContainerInput;
