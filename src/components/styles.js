import styled from 'styled-components';

export const DropdownContainer = styled.div`
  display: flex;
  flex-direction: column;
  a {
    height: 43px !important;
  }
  a, span{
    color: #888888;
  }
  .dropdown {
    margin-top: 0px;
  }
  input{
    color: #888888;
    background: #eeeeee;
  }
  input:focus,
  input:hover{
    background: #fff;
    color: #888888;
  }
`;

export const ContentDiv = styled.div`
  display: contents;
`;

export const ActionsContainer = styled.div`
  display: flex;
  .smallMargin {
    margin-bottom: 10px;
  }
  button {
    cursor: pointer;
    margin-right: 10px;
  }
`;

export const IconsTransfer = styled.div`
  box-sizing: border-box;
`;
