import styled from 'styled-components';

export const Content = styled.div`
  max-width: 1200px;
  margin: 4rem auto;
`;

export const ContentAddButton = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  max-width: 600px;
  & input {
    margin: 1rem;
  }
`;